![Build Status](https://gitlab.com/pages/pastaclan/nanoc/badges/master/pipeline.svg)

---

pastaclan website for culurjonis 
---

**table of contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [gitLab ci](#gitlab-ci)
- [building locally](#building-locally)
- [gitLab user or group pages](#gitlab-user-or-group-pages)
- [did you fork this project?](#did-you-fork-this-project)
- [troubleshooting](#troubleshooting)

## gitLab CI

this project's static pages are built by [gitLab ci][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: ruby:2.6

pages:
  script:
  - bundle install -j4
  - nanoc
  artifacts:
    paths:
    - public
  only:
  - master
```

## building locally

to work locally with this project, you'll have to follow the steps below:

1. [install](https://nanoc.ws/doc/installation/) nanoc
1. install all the dependencies: `bundle install`
1. generate the website: `nanoc`
1. preview your project: `nanoc view`
1. add content

read more at nanoc's [documentation](https://nanoc.ws/doc/)

## gitLab user or group pages

to use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **settings**.

read more about [user/group pages][userpages] and [project pages][projpages].

## did you fork this project?

if you forked this project for your own use, please go to your project's
**settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## troubleshooting

1. css is missing! that means two things:

    either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[nanoc]: http://nanoc.ws/
[install]: http://nanoc.ws/doc/installation/
[documentation]: http://nanoc.ws/doc/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

